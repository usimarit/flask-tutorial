from flask import Flask, request, jsonify
from flask_api import FlaskAPI
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()


def create_app():
    from app.models import User#, user_schema, users_schema
    app = FlaskAPI(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:abc123456@localhost:3306/flask_tutorial'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['TESTING'] = True
    db.init_app(app)
    ma.init_app(app)

    @app.route('/')
    def hello_world():
        return 'Hello World!'

    # endpoint to create new user
    @app.route('/user', methods=['POST'])
    def add_user():
        username = str(request.data.get('username', ''))
        email = str(request.data.get('email'))

        new_user = User(username, email)

        db.session.add(new_user)
        db.session.commit()

        response = jsonify({
            'id': new_user.id,
            'username': new_user.username,
            'email': new_user.email
        })
        response.status_code = 201
        return response

    # endpoint to show all users
    @app.route('/user', methods=['GET'])
    def get_user():
        all_users = User.query.all()
        result = []
        for u in all_users:
            obj = {
                'id': u.id,
                'username': u.username,
                'email': u.email
            }
            result.append(obj)
        response = jsonify(result)
        response.status_code = 200
        return response

    # endpoint to get user detail by id
    @app.route('/user/<id>', methods=['GET'])
    def user_detail(id):
        user = User.query.get(id)
        response = jsonify({
            'id': user.id,
            'username': user.username,
            'email': user.email
        })
        response.status_code = 200
        return response

    # endpoint to update user
    @app.route('/user/<id>', methods=['PUT'])
    def user_update(id):
        user = User.query.get(id)
        username = str(request.data.get('username', ''))
        email = str(request.data.get('email'))

        user.email = email
        user.username = username

        db.session.commit()
        response = jsonify({
            'id': user.id,
            'username': user.username,
            'email': user.email
        })
        response.status_code = 200
        return response

    # endpoint to delete user
    @app.route('/user/<id>', methods=['DELETE'])
    def user_delete(id):
        user = User.query.get(id)
        db.session.delete(user)
        db.session.commit()

        return {
            "message": "user {} deleted successfully".format(user.id)
        }, 200

    return app
