import unittest
import os
import json
from app import create_app, db


class UserTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client
        self.userList = {'username': 'john', 'email': 'john@teko.vn'}

    def test_creation(self):
        res = self.client().post('/user', data=self.userList)
        self.assertEqual(res.status_code, 201)
        print(str(res.data))

    def test_api_get_all(self):
        res = self.client().get('/user')
        self.assertEqual(res.status_code, 200)
        print(str(res.data))


if __name__ == '__main__':
    unittest.main()
